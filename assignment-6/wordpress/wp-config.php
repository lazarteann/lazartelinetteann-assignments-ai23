<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'word' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'WVIlpTC*=6=vN?e|[V7a+ztFRPO#Pc$jDw^<[L%EC)UP(B_D^:eYP9K~nQAbe}*d' );
define( 'SECURE_AUTH_KEY',  '(gB^Bs`V^|1YeaL4l6(ym^L|2)oS@m,6{R&- 6aL]4oVZ22,#O,hCjwSR`|/eCB:' );
define( 'LOGGED_IN_KEY',    '88vZ?Scd}+536E/PoT4{KjU]tNAgije~aTs:IyG;}/[zru)_~u8]tKGP$4naD^Tq' );
define( 'NONCE_KEY',        'T{fvO8WySS1mT]&Zt2WS/C)Ret;P6q55Ci^B,Yq_z#Akq2JqM^W/yx|G5b%(sDki' );
define( 'AUTH_SALT',        'ccPO4!CW{[h@j&gxk_jv>1J}9 ?&+FC~[TV}/!97lPN3q@9X&JX{~Z=L;EYc9_+V' );
define( 'SECURE_AUTH_SALT', 'Q+*T Xkb*SQZ63xADGcVTT#u/N-3EFIGG&=zT|7h|zf6/Yl/3pI:6o:.X]$0duC5' );
define( 'LOGGED_IN_SALT',   'tEtr/|PtW,Z[;p~$#H%=PL(UqMU6bRMu]%}PhU(7 ]KzWVHJ FfzxI?8(mh,ul9]' );
define( 'NONCE_SALT',       'ihYjbSwg5<ir`yIwnb7O!n$HCr@#<q?10lz_XEzx(0vy>2}upj^JDa~P!y,[Q%qE' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
