mplémentaires.</val>
		</str>
		<str name="idis" translate="yes">
			<val>Votre ID Adobe est : {0}</val>
		</str>
		<str name="idnew" translate="yes">
			<val>Votre ID Adobe est : {0}</val>
		</str>
		
		<str name="email" translate="yes">
			<val>Un courrier électronique a été envoyé pour vérifier votre adresse électronique. La vérification est nécessaire avant d'utiliser les services Adobe CS Live.</val>
		</str>
		<str name="videos" translate="yes">
			<val>Visionner des didacticiels vidéo</val>
		</str>
		<str name="explore" translate="yes">
			<val>Découvrir les services Adobe CS LIVE</val>
		</str>
		<str name="videolink" translate="yes">
			<val>http://www.adobe.com/go/adobetv_howto_fr</val>
		</str>		
		<str name="serviceslink" translate="yes">
			<val>http://www.adobe.com/go/cslive_fr</val>
		</str>	
	</set> 
	
	
	<set name="upgrade">
		<str name="title" translate="yes">
			<val>Numéro de série : mise à niveau</val>
		</str>
		<str name="instruct1" translate="yes">
			<val>Le numéro d