o s   d e   e s t e   p a n e l   p r o c e d e n   d e l   e s t � n d a r   d e   E x t e n s i � n   I P T C   ( h t t p : / / w w w . i p t c . o r g / p h o t o m e t a d a t a ) . \ n *   E s t o s   c a m p o s   s e   h a n   i n c l u i d o   a   p a r t i r   d e l   e s t � n d a r   P L U S   ( h t t p : / / w w w . u s e p l u s . o r g ) . "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / P e r s o n I n I m a g e = P e r s o n a   e n   l a   i m a g e n : "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / P e r s o n I n I m a g e _ d e s c r i p t i o n = I n t r o d u z c a   e l   n o m b r e   d e   l a   p e r s o n a   q u e   a p a r e c e   e n   e s t a   i m a g e n . "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / P r o v i c e S t a t e = P r o v i n c i a / E s t a d o : "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / P r o v i c e S t a t e 2 = P r o v i n c i a / E s t a d o "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / P r o v i c e S t a t e _ d e s c r i p t i o n = I n t r o d u z c a   e l   n o m b r e   d e   l a   p r o v i n c i a   o   e s t a d o . "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / R e g I t e m I d = I d e n t i f i c a d o r   d e   e l e m e n t o "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / R e g I t e m I d _ d e s c r i p t i o n = I n t r o d u z c a   e l   i d e n t i f i c a d o r   � n i c o   c r e a d o   p o r   u n   r e g i s t r o   y   a p l i c a d o   p o r   e l   a u t o r   d e   l a   i m a g e n   d i g i t a l .   E s t e   v a l o r   n o   d e b e   c a m b i a r s e   d e s p u � s   d e   s u   a p l i c a c i � n . "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / R e g O r g I d = I d e n t i f i c a d o r   d e   o r g a n i z a c i � n "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / R e g O r g I d _ d e s c r i p t i o n = I n t r o d u z c a   e l   i d e n t i f i c a d o r   d e l   r e g i s t r o   q u e   h a   p r o p o r c i o n a d o   e l   I D   d e   i m a g e n   d e   r e g i s t r o   c o r r e s p o n d i e n t e . "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / R e g i s t r y I d = E n t r a d a   d e   r e g i s t r o : "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / R e g i s t r y I d _ d e s c r i p t i o n = I n t r o d u z c a   l o s   i d e n t i f i c a d o r e s   d e   e s t a   i m a g e n   y   d e l   r e g i s t r o   q u e   p r o p o r c i o n a   e l   I D   d e   i m a g e n . "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / S u b l o c a t i o n = L u g a r   ( s u b l o c a l i z a c i � n ) : "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / S u b l o c a t i o n 2 = L u g a r   ( s u b l o c a l i z a c i � n ) "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / S u b l o c a t i o n _ d e s c r i p t i o n = I n t r o d u z c a   e l   n o m b r e   d e   l a   s u b l o c a l i z a c i � n . "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / W o r l d R e g i o n = R e g i � n   d e l   m u n d o : "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / W o r l d R e g i o n 2 = R e g i � n   d e l   m u n d o "  
 " $ $ $ / x m p / I p t c 4 x m p E x t / W o r l d R e g i o n _ d e s c r i p t i o n = I n t r o d u z c a   e l   n o m b r e   d e   l a   r e g i � n   d e l   m u n d o . "  
 " $ $ $ / x m p / I p t c 4 x m p E x t u s / D i g i t a l S o u r c e T y p e _ d e s c r i p t i o n = S e l e c c i o n e   u n o   d e   l o s   v a l o r e s   p a r a   i d e n t i f i c a r   e l   t i p o   d e   f u e n t e   d e   l a   i m a g e n   d i g i t a l   a   p a r t i r   d e l   v o c a b u l a r i o   c o n t r o l a d o "  
 " $ $ $ / x m p / p l u s / C o p y r i g h t O w n e r = D u e � o   d e l   c o p y r i g h t : "  
 " $ $ $ / x m p / p l u s / C o p y r i g h t O w n e r I D = I d e n t i f i c a d o r "  
 " $ $ $ / x m p / p l u s / C o p y r i g h t O w n e r I D _ d e s c r i p t i o n = P L U S - I D   o p c i o n a l   q u e   i d e n t i f i c a   a   c a d a   d u e � o   d e l   c o p y r i g h t . "  
 " $ $ $ / x m p / p l u s / C o p y r i g h t O w n e r N a m e = === 0x80) {
				$character = $value;
				$length = 1;
				$remaining = 0;
			}
			// Two byte sequence:
			elseif (($value & 0xE0) === 0xC0) {
				$character = ($value & 0x1F) << 6;
				$length = 2;
				$remaining = 1;
			}
			// Three byte sequence:
			elseif (($value & 0xF0) === 0xE0) {
				$character = ($value & 0x0F) << 12;
				$length = 3;
				$remaining = 2;
			}
			// Four byte sequence:
			elseif (($value & 0xF8) === 0xF0) {
				$character = ($value & 0x07) << 18;
				$length = 4;
				$remaining = 3;
			}
			// Invalid byte:
			else {
				throw new Requests_Exception('Invalid Unicode codepoint', 'idna.invalidcodepoint', $value);
			}

			if ($remaining > 0) {
				if ($position + $length > $strlen) {
					throw new Requests_Exception('Invalid Unicode codepoint', 'idna.invalidcodepoint', $character);
				}
				for ($position++; $remaining > 0; $position++) {
					$value = ord($input[$position]);

					// If it is invalid, count the sequence as invalid and reprocess the current byte:
					if (($value & 0xC0) !== 0x80) {
						throw new Requests_Exception('Invalid Unicode codepoint', 'idna.invalidcodepoint', $character);
					}

					$character |= ($value & 0x3F) << (--$remaining * 6);
				}
				$position--;
			}

			if (
				// Non-shortest form sequences are invalid
				   $length > 1 && $character <= 0x7F
				|| $length > 2 && $character <= 0x7FF
				|| $length > 3 && $character <= 0xFFFF
				// Outside of range of ucschar codepoints
				// Noncharacters
				|| ($character & 0xFFFE) === 0xFFFE
				|| $character >= 0xFDD0 && $character <= 0xFDEF
				|| (
					// Everything else not in ucschar
					   $character > 0xD7FF && $character < 0xF900
					|| $character < 0x20
					|| $character > 0x7E && $character < 0xA0
					|| $character > 0xEFFFD
				)
			) {
				throw new Requests_Exception('Invalid Unicode codepoint', 'idna.invalidcodepoint', $character);
			}

			$codepoints[] = $character;
		}

		return $codepoints;
	}

	/**
	 * RFC3492-compliant encoder
	 *
	 * @internal Pseudo-code from Section 6.3 is commented with "#" next to relevant code
	 * @throws Requests_Exception On character outside of the domain (never happens with Punycode) (`idna.character_outside_domain`)
	 *
	 * @param string $input UTF-8 encoded string to encode
	 * @return string Punycode-encoded string
	 */
	public static function punycode_encode($input) {
		$output = '';
#		let n = initial_n
		$n = self::BOOTSTRAP_INITIAL_N;
#		let delta = 0
		$delta = 0;
#		let bias = initial_bias
		$bias = self::BOOTSTRAP_INITIAL_BIAS;
#		let h = b = the number of basic code points in the input
		$h = $b = 0; // see loop
#		copy them to the output in order
		$codepoints = self::utf8_to_codepoints($input);
		$extended = array();

		foreach ($codepoints as $char) {
			if ($char < 128) {
				// Character is valid ASCII
				// TODO: this should also check if it's valid for a URL
				$output .= chr($char);
				$h++;
			}
			// Check if the character is non-ASCII, but below initial n
			// This never occurs for Punycode, so ignore in coverage
			// @codeCoverageIgnoreStart
			elseif ($char < $n) {
				throw new Requests_Exception('Invalid character', 'idna.character_outside_domain', $char);
			}
			// @codeCoverageIgnoreEnd
			else {
				$extended[$char] = true;
			}
		}
		$extended = array_keys($extended);
		sort($extended);
		$b = $h;
#		[copy them] followed by a delimiter if b > 0
		if (strlen($output) > 0) {
			$output .= '-';
		}
#		{if the input contains a non-basic code point < n then fail}
#		while h < length(input) do begin
		while ($h < count($codepoints)) {
#			let m = the minimum code point >= n in the input
			$m = array_shift($extended);
			//printf('next code point to insert is %s' . PHP_EOL, dechex($m));
#			let delta = delta + (m - n) * (h + 1), fail on overflow
			$delta += ($m - $n) * ($h + 1);
#			let n = m
			$n = $m;
#			for each code point c in the input (in order) do begin
			for ($num = 0; $num < count($codepoints); $num++) {
				$c = $codepoints[$num];
#				if c < n then increment delta, fail on overflow
				if ($c < $n) {
					$delta++;
				}
#				if c == n then begin
				elseif ($c === $n) {
#					let q = delta
					$q = $delta;
#					for k = base to infinity in steps of base do begin
					for ($k = self::BOOTSTRAP_BASE; ; $k += self::BOOTSTRAP_BASE) {
#						let t = tmin if k <= bias {+ tmin}, or
#								tmax if k >= bias + tmax, or k - bias otherwise
						if ($k <= ($bias + self::BOOTSTRAP_TMIN)) {
							$t = self::BOOTSTRAP_TMIN;
						}
						elseif ($k >= ($bias + self::BOOTSTRAP_TMAX)) {
							$t = self::BOOTSTRAP_TMAX;
						}
						else {
							$t = $k - $bias;
						}
#						if q < t then break
						if ($q < $t) {
							break;
						}
#						output the code point for digit t + ((q - t) mod (base - t))
						$digit = $t + (($q - $t) % (self::BOOTSTRAP_BASE - $t));
						$output .= self::digit_to_char($digit);
#						let q = (q - t) div (base - t)
						$q = floor(($q - $t) / (self::BOOTSTRAP_BASE - $t));
#					end
					}
#					output the code point for digit q
					$output .= self::digit_to_char($q);
#					let bias = adapt(delta, h + 1, test h equals b?)
					$bias = self::adapt($delta, $h + 1, $h === $b);
#					let delta = 0
					$delta = 0;
#					increment h
					$h++;
#				end
				}
#			end
			}
#			increment delta and n
			$delta++;
			$n++;
#		end
		}

		return $output;
	}

	/**
	 * Convert a digit to its respective character
	 *
	 * @see https://tools.ietf.org/html/rfc3492#section-5
	 * @throws Requests_Exception On invalid digit (`idna.invalid_digit`)
	 *
	 * @param int $digit Digit in the range 0-35
	 * @return string Single character corresponding to digit
	 */
	protected static function digit_to_char($digit) {
		// @codeCoverageIgnoreStart
		// As far as I know, this never happens, but still good to be sure.
		if ($digit < 0 || $digit > 35) {
			throw new Requests_Exception(sprintf('Invalid digit %d', $digit), 'idna.invalid_digit', $digit);
		}
		// @codeCoverageIgnoreEnd
		$digits = 'abcdefghijklmnopqrstuvwxyz0123456789';
		return substr($digits, $digit, 1);
	}

	/**
	 * Adapt the bias
	 *
	 * @see https://tools.ietf.org/html/rfc3492#section-6.1
	 * @param int $delta
	 * @param int $numpoints
	 * @param bool $firsttime
	 * @return int New bias
	 */
	protected static function adapt($delta, $numpoints, $firsttime) {
#	function adapt(delta,numpoints,firsttime):
#		if firsttime then let delta = delta div damp
		if ($firsttime) {
			$delta = floor($delta / self::BOOTSTRAP_DAMP);
		}
#		else let delta = delta div 2
		else {
			$delta = floor($delta / 2);
		}
#		let delta = delta + (delta div numpoints)
		$delta += floor($delta / $numpoints);
#		let k = 0
		$k = 0;
#		while delta > ((base - tmin) * tmax) div 2 do begin
		$max = floor(((self::BOOTSTRAP_BASE - self::BOOTSTRAP_TMIN) * self::BOOTSTRAP_TMAX) / 2);
		while ($delta > $max) {
#			let delta = delta div (base - tmin)
			$delta = floor($delta / (self::BOOTSTRAP_BASE - self::BOOTSTRAP_TMIN));
#			let k = k + base
			$k += self::BOOTSTRAP_BASE;
#		end
		}
#		return k + (((base - tmin + 1) * delta) div (delta + skew))
		return $k + floor(((self::BOOTSTRAP_BASE - self::BOOTSTRAP_TMIN + 1) * $delta) / ($delta + self::BOOTSTRAP_SKEW));
	}
}