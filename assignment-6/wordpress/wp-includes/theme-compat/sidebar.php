Param1>
        <stCamera:VignetteModelParam2>-1.411179</stCamera:VignetteModelParam2>
        <stCamera:VignetteModelParam3>5.3429</stCamera:VignetteModelParam3>
       </stCamera:VignetteModel>
      </stCamera:PerspectiveModel>
     </rdf:li>
     <rdf:li rdf:parseType="Resource">
      <stCamera:Author>Adobe Systems, Inc.</stCamera:Author>
      <stCamera:Make>SAMSUNG</stCamera:Make>
      <stCamera:Model>NX10</stCamera:Model>
      <stCamera:UniqueCameraModel>Samsung NX10</stCamera:UniqueCameraModel>
      <stCamera:CameraRawProfile>True</stCamera:CameraRawProfile>
      <stCamera:LensID>1</stCamera:LensID>
      <stCamera:Lens>Samsung 30mm f/2</stCamera:Lens>
      <stCamera:LensInfo>30/1 30/1 2/1 2/1</stCamera:LensInfo>
      <stCamera:CameraPrettyName>Samsung NX10</stCamera:CameraPrettyName>
      <stCamera:LensPrettyName>Samsung 30mm f/2</stCamera:LensPrettyName>
      <stCamera:ProfileName>Adobe (Samsung 30mm f/2)</stCamera:ProfileName>
      <stCamera:SensorFormatFactor>1.54</stCamera:SensorFormatFactor>
      <stCamera:ImageWidth>4686</stCamera:ImageWidth>
      <stCamera:ImageLength>3122</stCamera:ImageLength>
      <stCamera:FocalLength>30</stCamera:FocalLength>
      <stCamera:FocusDistance>3</stCamera:FocusDistance>
      <stCamera:ApertureValue>8</stCamera:ApertureValue>
      <stCamera:PerspectiveModel rdf:parseType="Resource">
       <stCamera:Version>2</stCamera:Version>
       <stCamera:FocalLengthX>1.310008</stCamera:FocalLengthX>
       <stCamera:FocalLengthY>1.310008</stCamera:FocalLengthY>
       <stCamera:RadialDistortParam1>-0.128654</stCamera:RadialDistortParam1>
       <stCamera:RadialDistortParam2>0.353593</stCamera:RadialDistortParam2>
       <stCamera:RadialDistortParam3>-0.858056</stCamera:RadialDistortParam3>
       <stCamera:VignetteModel rdf:parseType="Resource">
        <stCamera:FocalLengthX>1.310008</stCamera:FocalLengthX>
        <stCamera:FocalLengthY>1.310008</stCamera:FocalLengthY>
        <stCamera:VignetteModelParam1>-1.131186</stCamera:VignetteModelParam1>
        <stCamera:VignetteModelParam2>-1.130483</stCamera:VignetteModelParam2>
        <stCamera:VignetteModelParam3>4.749982</stCamera:VignetteModelParam3>
       </stCamera:VignetteModel>
      </stCamera:PerspectiveModel>
     </rdf:li>
     <rdf:li rdf:parseType="Resource">
      <stCamera:Author>Adobe Systems, Inc.</stCamera:Author>
      <stCamera:Make>SAMSUNG</stCamera:Make>
      <stCamera:Model>NX10</stCamera:Model>
      <stCamera:UniqueCameraModel>Samsung NX10</stCamera:UniqueCameraModel>
      <stCamera:CameraRawProfile>True</stCamera:CameraRawProfile>
      <stCamera:LensID>1</stCamera:LensID>
      <stCamera:Lens>Samsung 30mm f/2</stCamera:Lens>
      <stCamera:LensInfo>30/1 30/1 2/1 2/1</stCamera:LensInfo>
      <stCamera:CameraPrettyName>Samsung NX10</stCamera:CameraPrettyName>
      <stCamera:LensPrettyName>Samsung 30mm f/2</stCamera:LensPrettyName>
      <stCamera:ProfileName>Adobe (Samsung 30mm f/2)</stCamera:ProfileName>
      <stCamera:SensorFormatFactor>1.54</stCamera:SensorFormatFactor>
      <stCamera:ImageWidth>4686</stCamera:ImageWidth>
      <stCamera:ImageLength>3122</stCamera:ImageLength>
      <stCamera:FocalLength>30</stCamera:FocalLength>
      <stCamera:FocusDistance>3</stCamera:FocusDistance>
      <stCamera:ApertureValue>8.918863</stCamera:ApertureValue>
      <stCamera:PerspectiveModel rdf:parseType="Resource">
       <stCamera:Version>2</stCamera:Version>
       <stCamera:FocalLengthX>1.310008</stCamera:FocalLengthX>
       <stCamera:FocalLengthY>1.310008</stCamera:FocalLengthY>
       <stCamera:RadialDistortParam1>-0.128654</stCamera:RadialDistortParam1>
       <stCamera:RadialDistortParam2>0.353593</stCamera:RadialDistortParam2>
       <stCamera:RadialDistortParam3>-0.858056</stCamera:RadialDistortParam3>
       <stCamera:VignetteModel rdf:parseType="Resource">
        <stCamera:FocalLengthX>1.310008</stCamera:FocalLengthX>
        <stCamera:FocalLengthY>1.310008</stCamera:FocalLengthY>
        <stCamera:VignetteModelParam1>-1.141179</stCamera:VignetteModelParam1>
        <stCamera:VignetteModelParam2>-0.906206</stCamera:VignetteModelParam2>
        <stCamera:VignetteModelParam3>