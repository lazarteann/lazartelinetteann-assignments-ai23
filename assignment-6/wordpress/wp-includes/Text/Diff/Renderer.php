t-size:8.0pt;mso-bidi-font-size:10.0pt;
font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:4.0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Tahoma'>컴퓨터를 도난당했거나 컴퓨터가 손상된 경우에는 어떻게 됩니까? 복구할 수 없습니까?<o:p></o:p></span></b></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'>컴퓨터를 도난당했거나, 컴퓨터가 복구할 수 없을 정도로 손상되었거나, 하드 드라이브의 이미지를 완전히 다시 만든 경우에는 활성화가 손실됩니다. 그러한 경우에는 새 컴퓨터에서 </span><st1:PersonName><span style='font-size:10.0pt; font-family:Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> 제품을 설치하고 사용할 수 있으며, 이때 두 대의 컴퓨터에서 이미 활성화된 경우에는 문제가 자동으로 검색됩니다. 이전 컴퓨터를 더 이상 사용할 수 없는 경우일지라도 활성화 프로세스를 통해 새로 활성화를 진행할 수 있습니다.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt;
font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt;
font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:4.0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Tahoma'>일련 번호가 거부되었습니다. 이는 무엇을 의미합니까?<o:p></o:p></span></b></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'>활성화를 처리하는 동안, </span><st1:PersonName><span style='font-size: 10.0pt;font-family:Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> 활성화 응용 프로그램은 제공된 일련 번호가 잘못되었거나(정품이 아님) 명시적으로 해지되었음을 확인할 수 있습니다. 그러한 경우 사용자는 메시지를 받게 되며 </span><st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> 소프트웨어 제품에 대한 새 일련 번호를 입력할 수 있습니다.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt;
font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt;
font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:4.0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Tahoma'>저희 회사에서는 </span></b><st1:PersonName><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family: Tahoma'>Adobe</span></b></st1:PersonName><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Tahoma'> 사용권을 볼륨 계약 하에 구입했습니다. 이때 <span class=GramE>활성화가</span> 미치는 영향은 무엇입니까?<o:p></o:p></span></b></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'>제품 활성화는 대부분 개별 정품 버전의 </span><st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> Acrobat 9에 적용되며 일반적으로 </span><st1:PersonName><span style='font-size:10.0pt;font-family: Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> Open Options 프로그램의 일부가 아닙니다. 그러나 일부 지역에서는 활성화가 특정 볼륨 사용권 프로그램에 적용될 수도 있습니다. </span><st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> 소프트웨어 제품을 주문한 볼륨 사용권 고객은 사용권 프로그램별 CD 세트를 주문해야 합니다. 의문 사항이 있는 경우에는 </span><st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> 공인 대리점 또는 고객 지원 센터 담당자에게 문의하거나 </span><st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'></span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> <a href="http://www.adobe.com/kr/aboutadobe/openoptions/">Adobe Open Options</a>를 참조하십시오.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:4.0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Tahoma'>더 자세한 내용을 알고 싶으면 어디에 문의해야 합니까?<o:p></o:p></span></b></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'><a href="http://www.adobe.com/kr/activation/phonenumbers.html">Adobe 고객 지원 센터</a>에 문의하면 활성화 문제에 대한 도움을 받으실 수 있습니다.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'>활성화에 대한 추가 정보 및 자세한 내용은 <a href="http://www.adobe.com/go/activation_kr">Adobe 정품 인증 센터</a>에서 볼 수 있습니다.<u><span style='color:blue'><o:p></o:p></span></u></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'><span
style='mso-spacerun:yes'> </span><o:p></o:p></span></p>

</div>

</body>

</html>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              