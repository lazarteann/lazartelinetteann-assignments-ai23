p l a y L a n g u a g e N a m e / h r = K r o a t i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / h r _ H R = K r o a t i s c h   ( K r o a t i e n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / h u = U n g a r i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / h u _ H U = U n g a r i s c h   ( U n g a r n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / h y = A r m e n i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / h y _ A M = A r m e n i s c h   ( A r m e n i e n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / h y _ A M _ R E V I S E D = A r m e n i s c h   ( A r m e n i e n ,   R e v i d i e r t ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / i d = I n d o n e s i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / i d _ I D = I n d o n e s i s c h   ( I n d o n e s i e n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / i s = I s l � n d i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / i s _ I S = I s l � n d i s c h   ( I s l a n d ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / i t = I t a l i e n i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / i t _ C H = I t a l i e n i s c h   ( S c h w e i z ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / i t _ I T = I t a l i e n i s c h   ( I t a l i e n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / i t _ I T _ P R E E U R O = I t a l i e n i s c h   ( I t a l i e n ,   P R E E U R O ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / j a = J a p a n i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / j a _ J P = J a p a n i s c h   ( J a p a n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / j a _ J P _ T R A D I T I O N A L = J a p a n i s c h   ( J a p a n ,   T R A D I T I O N A L ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k k = K a s a c h i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k k _ K Z = K a s a c h i s c h   ( K a s a c h s t a n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k l = G r � n l � n d i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k l _ G L = G r � n l � n d i s c h   ( G r � n l a n d ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k n = K a n n a d a "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k n _ I N = K a n n a d a   ( I n d i e n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k o = K o r e a n i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k o _ K R = K o r e a n i s c h   ( R e p u b l i k   K o r e a ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k o k = k o k "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k o k _ I N = k o k   ( I n d i e n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k w = k w "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / k w _ G B = k w   ( V e r e i n i g t e s   K � n i g r e i c h ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / l t = L i t a u i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / l t _ L T = L i t a u i s c h   ( L i t a u e n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / l v = L e t t i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / l v _ L V = L e t t i s c h   ( L e t t l a n d ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / m k = M a z e d o n i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / m k _ M K = M a z e d o n i s c h   ( M a z e d o n i e n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / m r = M a r a t h i "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / m r _ I N = M a r a t h i   ( I n d i e n ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / m s = M a l a i i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / m s _ B N = M a l a i i s c h   ( B r u n e i   D a r u s s a l a m ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / m s _ M Y = M a l a i i s c h   ( M a l a y s i a ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / m t = M a l t e s i s c h "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / m t _ M T = M a l t e s i s c h   ( M a l t a ) "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / n b = N o r w e g i s c h   B o k m � l "  
 " $ $ $ / L i l o / D i s p l a y L a n g u a g e N a m e / n b _ N O = N o r w e g i s c h   B o k m � l   ( N o r w e g e n ) " 