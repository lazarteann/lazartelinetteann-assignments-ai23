>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:4.0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Tahoma'>Vai aktiviz�cijas laik� tiek p�rs�t�ti personiskie dati?<o:p></o:p></span></b></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'>N�. Aktiviz�cija ir piln�gi anon�ma un nek�da personiski identific�jama inform�cija netiek p�rs�t�ta. Katrai </span><st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> programmat�rai pie��irtais unik�lais s�rijas numurs tiek apvienots ar �datora identifikatoru�. Aktiviz�jot produktu, �is datora identifikators, produkta s�rijas numurs un datora virknes nosaukums tiek p�rs�t�ts uz </span><st1:PersonName><span style='font-size:10.0pt; font-family:Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'>, lai nodro�in�tu, ka ��s programmat�ras ori�in�la katra kopija netiek aktiviz�ta vair�k rei�u nek� at�auts. Aktiviz�cijas sist�ma neuzkr�j, nep�rs�ta un neizmanto nek�du personisko inform�ciju.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt; font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt; font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:4.0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Tahoma'>Esmu jau iepriek� izmantojis </span></b><st1:PersonName><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family: Tahoma'>Adobe</span></b></st1:PersonName><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Tahoma'> programmat�ru ar aktiviz�ciju. Kas tagad ir main�jies?<o:p></o:p></span></b></p>

<p class=MsoNormal><st1:PersonName><span style='font-size:10.0pt;font-family: Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> vienk�r�o aktiviz��anu. Agr�k aktiviz�cija bija prec�zi formul�ts process, kuru vajadz�ja veikt 30 dienu laik� kopp pirm�s produkta izmanto�anas reizes. S�kot ar Acrobat 9, programmat�ra tiek aktiviz�ta autom�tiski, l�dzko nosaka akt�vu savienojumu ar internetu. Aktiviz�cija netrauc�jot notiek fon�. Aktiviz�cija pa t�lruni turpm�k vairs neb�s nepiecie�ama vai pat netiks pied�v�ta k� opcija.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt; font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt; font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:4.0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Tahoma'>K� var aktiviz�t programmat�ru, ja dators nav savienots ar internetu?<o:p></o:p></span></b></p>

<p class=MsoNormal><span style='font-size:10.0pt;font-family:Tahoma'> </span><st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'>Adobe</span></st1:PersonName><span style='font-size:10.0pt;font-family:Tahoma'> programmat�ra nosaka, kad pieejams savienojums ar internetu, un veic aktiviz�ciju autom�tiski. Ja savienojums nav pieejams, produkts turpin�s norm�li darboties un m��in�s aktiviz�ties n�kam�s darba sesijas laik�.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt; font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt; font-family:Tahoma'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:4.0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;font-family:Ta