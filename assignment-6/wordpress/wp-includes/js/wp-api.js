 incluída com seu tráfego de navegação. Qualquer efeito depende de o website responder ou não à solicitação e da forma como a solicitação é interpretada. Por exemplo, alguns websites podem responder a essa solicitação mostrando anúncios que não sejam baseados em outros websites que você tenha visitado. Muitos websites ainda coletam e usam seus dados de navegação, por exemplo, para melhorar a segurança, fornecer conteúdo, serviços, anúncios e recomendações no website e para gerar estatísticas de relatórios. <a target="_blank" href="$1">Saiba mais</a>Ativa o acesso verificadoManter Wi-Fi ativado durante a suspensão.Gerenciar certificadosGerencia configurações e certificados HTTPS/SSLConfigurações de conteúdoConfigurações do siteControla quais informações os sites podem usar e quais conteúdos eles podem mostrarLimpar dadosLimpar dados de navegaçãoLimpa o histórico, os cookies, o cache e muito maisMais configurações relacionadas à privacidade, segurança e coleta de dadosRedefinir configuraçõesRestaurar configurações para os padrões originaisAlgumas configurações foram redefinidasO Google Chrome detectou que algumas das suas configurações foram corrompidas por outro programa e as redefiniu para os padrões originais.Redefinir todas as configuraçõesRedefinir e limparLimpe o computadorMecanismo de pesquisaO mecanismo de pesquisa usado na <a target="_blank" href="$1">barra de endereço</a>Gerenciar mecanismos de pesquisaMecanismos de pesquisaAdicionar mecanismo de pesquisaEditar mecanismo de pesquisaMecanismos de pesquisa padrãoOutros mecanismos de pesquisaOutros mecanismos de pesquisa salvos serão exibidos aquiFerramentas de buscas adicionadas por extensõesPalavra-chaveURL de consultaURL com %s no lugar da consultaRemover da listaGerenciarincorporado em $1incorporado em qualquer hostCategoria de permissãoTodos os sitesVer permissões e dados armazenados em sitesClassificar porDados armazenados—AnúnciosDownloads automáticosSincronização em segundo planoÁrea de transferênciaPerguntar quando um site quiser ver os textos e imagens copiados para a área de transferênciaPerguntar quando um site quiser ver os textos e imagens copiados para a área de transferência (recomendado)Não permitir que sites vejam textos e imagens copiados para a área de transferênciaCookiesManipuladoresImagensJavaScriptFlashGerenciadores de pagamentoPermitir que sites instalem gerenciadores de pagamentoPermitir que sites instalem gerenciadores de pagamento (recomendado)Não permitir que nenhum site instale gerenciadores de pagamentoDocumentos em PDFFazer o download de arquivos PDF em vez de abri-los automaticamente no ChromePop-ups e redirecionamentosConteúdo protegidoIdentificadores de conteúdo protegidoPermitir que os sites reproduzam conteúdo protegido (recomendado)Alguns serviços de conteúdo usam identificadores exclusivos para autorizar o acesso a conteúdo protegidoPermitir identificadores de conteúdo protegido (pode ser necessário reiniciar o computador)Acesso a plug-in sem sandboxDispositivos MIDIPerguntar quando um site quiser usar mensagens exclusivas do sistema para acessar dispositivos MIDIPerguntar quando um site quiser usar mensagens exclusivas do sistema para acessar dispositivos MIDI (recomendado)Não permitir que nenhum site use mensagens exclusivas do sistema para acessar dispositivos MIDISomSensores de luz ou movimentoDispositivos USBPerguntar quando um site quiser acessar dispositivos USBPerguntar quando um site quiser acessar dispositivos USB (recomendado)Não permitir que nenhum site acesse dispositivos USBRemover o nível de zoomNíveis de zoomOs sites podem salvar e ler os dados dos cookiesPerguntar primeiroPerguntar primeiro (recomendado)Perguntar antes de acessarPerguntar antes de acessar (recomendado)Perguntar antes de enviarPerguntar antes de enviar (recomendado)Não mostrar nenhuma imagemExibir todosMostrar tudo (recomendado)Permitir que os sites salvem e leiam os dados de arquivos "cookies" - que armazenam temporariamente o que você visitou na rede.Permitir que os sites salvem e leiam os dados de arquivos "cookies" - que armazenam temporariamente o que você visitou na rede. (Recomendado)Impedir que sites executem FlashSuas configurações de Flash ficarão salvas até que você saia do Chrome.Executar apenas conteúdos importantes (recomendado)Executar apenas conteúdos importantesPermitir que sites fechados recentemente concluam o envio e o recebimento de dadosPermitir que sites fechados recentemente concluam o envio e o recebimento de dados (recomendado)Não permitir que sites fechados recentemente concluam o envio e o recebimento de dadosPermitir que os sites peçam para se tornarem manipuladores padrão de protocolosPermitir que os sites peçam para se tornar manipuladores padrão de protocolos (recomendado)Não permitir que qualquer site trabalhe com protocolosBloqueados em sites que mostram anúncios invasivos ou enganososBloqueados em sites que mostram anúncios invasivos ou enganosos (recomendado)Permitir o áudio dos sitesPermitir o áudio dos sites (recomendado)Silenciar sites com áudioPermitir que sites usem sensores de luz e movimentoImpedir que sites usem sensores de luz e movimentoPerguntar quando um site tentar fazer o download de arquivos automaticamente após o primeiro arquivoPerguntar quando um site tenta fazer o download de arquivos automaticamente após o primeiro arquivo (recomendado)Não permitir que nenhum site faça download de vários arquivos automaticamentePerguntar quando um site quiser usar um plug-in para acessar seu computadorPerguntar quando um site quiser usar um plug-in para acessar seu computador (recomendado)Não permita que nenhum site use um plug-in para acessar seu computadorPermitidoPermitido (recomendado)BloqueadoBloqueada (recomendado)PermitirBloquearLimpar ao sairPerguntar (padrão)Permitir (padrão)Automático (padrão)Bloquear (padrão)Desativar som (padrão)PerguntarUsoNenhum dado de usoPara alterar essa configuração, primeiro <a target="_blank" href="$1">ative os identificadores</a>No site, há exibição de anúncios invasivos ou enganososBloquear se o site mostrar anúncios invasivos ou enganososBloqueada temporariamente para proteger sua segurançaBloqueada para proteger sua privacidadeRedefinir permissõesTodos os dados de cookies e de sitesVer todos os cookies e dados do siteRemover tudoRemover todos os cookies mostradosLimpar dados de sitesDados armazenados localmente do $1Redefinir permissões de site para $1?Limpar dados do site?Os dados armazenados pelo $1 serão excluídos, exceto os cookies.Redefinir permissões do site?Os sites de $1 também serão redefinidos.Todos os dados armazenados pelo $1 e sites relacionados serão excluídos. Isso inclui cookies. Você será desconectado desses sites, inclusive nas guias abertas.Essa ação excluirá todos os dados armazenados no seu dispositivo para todos os sites exibidos. Quer continuar?Remover $1Limpar tudoRedefinirPesquisar cookiesBloquear cookies de terceirosImpedir sites de terceiros de salvar e ler dados de cookiesManter os dados locais só até você sair do navegadorConfigurações de armazenamento do Adobe Flash PlayerDefinir como padrãoApenas a sessão anônima atualSessão anônima atualSessão anônima atual ($1)Nenhum site teve o zoom aumentado ou diminuídoNenhum site adicionadoPermitir que Chrome defina quando os sites podem reproduzir sons (recomendado)Os sites que você visitar serão exibidos aquiNenhum site encontradoNenhum dispositivo USB encontrado[*.]example.comAdicionar um siteEditar siteSiteConteúdoDomínioCaminhoEnviar paraAcessível ao scriptCriado emExpira emCache de aplicativoDados do FlashManifesto{NUM_COOKIES,plural, =1{1 cookie}one{# cookie}other{# cookies}}DescriçãoOrigemTamanho em discoÚltima modificaçãoArmazenamento de banco de dadosArmazenamento localLicença de mídiaSistema de arquivosArmazenamento temporárioArmazenamento persistenteData do último acessoID do canalTipo de certificadoService WorkersShared WorkersArmazenamento de cacheGerenciar outras pessoasFoto do perfil do GoogleGerenciado por $1A tecnologia Google no ChromeSincronizado com $1DesativarA sincronização não está funcionandoComo essa conta é gerenciada por $1, seus favoritos, histórico, senhas e outras configurações serão excluídos desse dispositivo. No entanto, seus dados permanecerão armazenados na sua Conta do Google e poderão ser gerenciados no <a href="$2" target="_blank">Painel do Google</a>.Editar pessoaMostrar atalho na área de trabalhoDesativar sincronização e sair?Desativar a sincronização e a personalização?Limpar e continuarLimpar favoritos, histórico, senhas e mais deste dispositivo.Configurações salvas. Sincronização iniciada.Faça login para ter acesso aos favoritos, histórico, senhas e outras configurações em todos os seus dispositivos. Você também será conectado automaticamente aos seus serviços do Google.As alterações feitas nos seus favoritos, histórico, senhas e outras configurações não serão mais sincronizadas com sua Conta do Google. No entanto, os dados já existentes permanecerão armazenados na sua conta e será possível gerenciá-los no <a href="$1" target="_blank">Painel do Google</a>.Isso desconectará você das suas Contas do Google. As alterações feitas nos seus favoritos, histórico, senhas e outras configurações não serão mais sincronizadas com sua Conta do Google. No entanto, os dados já existentes continuarão armazenados na Conta do Google e poderão ser gerenciados no <a href="$1" target="_blank">Painel do Google</a>.Isso desconectará você das suas Contas do Google. Seus favoritos, histórico, senhas e outras configurações não serão mais sincronizados.Mostrar estatísticas do perfilRemover também deste dispositivo os dados já existentesLimpar e sairSincronizarOutros serviços do GoogleServiços do Google e de sincronizaçãoConfigurações de sincronização avançadasGerenciar sincronizaçãoAguarde…A sincronização começará assim que você sair desta páginaA sincronização será iniciada assim que você sair das configuraçõesCancelar sincronizaçãoCancelar sincronização?Ative a sincronização quando quiser nas configuraçõesVerifique sua conexão com a Internet. Se o problema continuar, tente sair e fazer login novamente.Sincronizar tudoGerenciar sua Conta do GoogleExtensõesEndereços, números de telefone e muito maisHistóricoGuias abertasSugestões de pesquisa do Google DriveAtividade e interaçõesUsa o conteúdo dos sites que você visita, além das interações e atividades do navegador, para personalizaçãoGerenciar dados sincronizados no Painel de controle do GoogleDados da "Sincronização do Chrome"Opções de criptografiaCriptografar senhas sincronizadas com seu nome de usuário e senha do GoogleCriptografar dados sincronizados com sua <a href="$1" target="_blank">senha longa de sincronização</a>. Isso não inclui formas de pagamento e endereços do Google Pay.Somente uma pessoa que tenha sua senha longa pode ler seus dados criptografados. A senha longa não é enviada para o Google ou armazenada por ele. Se você esquecer sua senha longa ou quiser alterar essa configuração, será necessário <a href="$1" target="_blank">redefinir a sincronização</a>.Para mudar essa configuração, <a href="$1" target="_blank">redefina a sincronização</a> e remova sua senha longa de sincronizaçãoPara ativar essa configuração, <a href="$1" target="_blank">redefina a sincronização</a> e remova sua senha longa de sincronizaçãoA senha longa não pode ficar em brancoInforme a mesma senha longa duas vezesA senha longa informada está incorretaSe você esqueceu sua senha longa ou deseja alterar essa configuração, <a href="$1" target="_blank">redefina a sincronização</a>.Confirmar senhaUsar configurações padrãoControlar como o histórico de navegação é usado para personalizar a Pesquisa, os anúncios e muito maisImportar favoritos e configuraçõesDe:Selecione os itens que serão importados:Carregando…Escolher arquivoImportarSeus favoritos e configurações estão prontosNão foi encontrado um navegador suportadoConteúdo da WebZoom da página:Tama							deferred.resolve( getModel );
						},
						error: function( getModel, response ) {
							deferred.reject( response );
						}
					} );
				} else {
					// Resolve with the embedded model.
					deferred.resolve( getModel );
				}

				// Return a promise.
				return deferred.promise();
			},

			/**
			 * Build a helper to retrieve a collection.
			 *
			 * @param  {string} parentModel      The parent model.
			 * @param  {string} collectionName   The name to use when constructing the collection.
			 * @param  {string} embedSourcePoint Where to check the embedds object for _embed data.
			 * @param  {string} embedIndex       An addiitonal optional index for the _embed data.
			 *
			 * @return {Deferred.promise}        A promise which resolves to the constructed collection.
			 */
			buildCollectionGetter = function( parentModel, collectionName, embedSourcePoint, embedIndex ) {
				/**
				 * Returns a promise that resolves to the requested collection
				 *
				 * Uses the embedded data if available, otherwises fetches the
				 * data from the server.
				 *
				 * @return {Deferred.promise} promise Resolves to a wp.api.collections[ collectionName ]
				 * collection.
				 */
				var postId, embeddeds, getObjects,
					classProperties = '',
					properties      = '',
					deferred        = jQuery.Deferred();

				postId    = parentModel.get( 'id' );
				embeddeds = parentModel.get( '_embedded' ) || {};

				// Verify that we have a valid post id.
				if ( ! _.isNumber( postId ) || 0 === postId ) {
					deferred.reject();
					return deferred;
				}

				// If we have embedded getObjects data, use that when constructing the getObjects.
				if ( ! _.isUndefined( embedSourcePoint ) && ! _.isUndefined( embeddeds[ embedSourcePoint ] ) ) {

					// Some embeds also include an index offset, check for that.
					if ( _.isUndefined( embedIndex ) ) {

						// Use the embed source point directly.
						properties = embeddeds[ embedSourcePoint ];
					} else {

						// Add the index to the embed source point.
						properties = embeddeds[ embedSourcePoint ][ embedIndex ];
					}
				} else {

					// Otherwise use the postId.
					classProperties = { parent: postId };
				}

				// Create the new getObjects collection.
				getObjects = new wp.api.collections[ collectionName ]( properties, classProperties );

				// If we didn’t have embedded getObjects, fetch the getObjects data.
				if ( _.isUndefined( getObjects.models[0] ) ) {
					getObjects.fetch( {
						success: function( getObjects ) {

							// Add a helper 'parent_post' attribute onto the model.
							setHelperParentPost( getObjects, postId );
							deferred.resolve( getObjects );
						},
						error: function( getModel, response ) {
							deferred.reject( response );
						}
					} );
				} else {

					// Add a helper 'parent_post' attribute onto the model.
					setHelperParentPost( getObjects, postId );
					deferred.resolve( getObjects );
				}

				// Return a promise.
				return deferred.promise();

			},

			/**
			 * Set the model post parent.
			 */
			setHelperParentPost = function( collection, postId ) {

				// Attach post_parent id to the collection.
				_.each( collection.models, function( model ) {
					model.set( 'parent_post', postId );
				} );
			},

			/**
			 * Add a helper function to handle post Meta.
			 */
			MetaMixin = {

				/**
				 * Get meta by key for a post.
				 *
				 * @param {string} key The meta key.
				 *
				 * @return {object} The post meta value.
				 */
				getMeta: function( key ) {
					var metas = this.get( 'meta' );
					return metas[ key ];
				},

				/**
				 * Get all meta key/values for a post.
				 *
				 * @return {object} The post metas, as a key value pair object.
				 */
				getMetas: function() {
					return this.get( 'meta' );
				},

				/**
				 * Set a group of meta key/values for a post.
				 *
				 * @param {object} meta The post meta to set, as key/value pairs.
				 */
				setMetas: function( meta ) {
					var metas = this.get( 'meta' );
					_.extend( metas, meta );
					this.set( 'meta', metas );
				},

				/**
				 * Set a single meta value for a post, by key.
				 *
				 * @param {string} key   The meta key.
				 * @param {object} value The meta value.
				 */
				setMeta: function( key, value ) {
					var metas = this.get( 'meta' );
					metas[ key ] = value;
					this.set( 'meta', metas );
				}
			},

			/**
			 * Add a helper function to handle post Revisions.
			 */
			RevisionsMixin = {
				getRevisions: function() {
					return buildCollectionGetter( this, 'PostRevisions' );
				}
			},

			/**
			 * Add a helper function to handle post Tags.
			 */
			TagsMixin = {

				/**
				 * Get the tags for a post.
				 *
				 * @return {Deferred.promise} promise Resolves to an array of tags.
				 */
				getTags: function() {
					var tagIds = this.get( 'tags' ),
						tags  = new wp.api.collections.Tags();

					// Resolve with an empty array if no tags.
					if ( _.isEmpty( tagIds ) ) {
						return jQuery.Deferred().resolve( [] );
					}

					return tags.fetch( { data: { include: tagIds } } );
				},

				/**
				 * Set the tags for a post.
				 *
				 * Accepts an array of tag slugs, or a Tags collection.
				 *
				 * @param {array|Backbone.Collection} tags The tags to set on the post.
				 *
				 */
				setTags: function( tags ) {
					var allTags, newTag,
						self = this,
						newTags = [];

					if ( _.isString( tags ) ) {
						return false;
					}

					// If this is an array of slugs, build a collection.
					if ( _.isArray( tags ) ) {

						// Get all the tags.
						allTags = new wp.api.collections.Tags();
						allTags.fetch( {
							data:    { per_page: 100 },
							success: function( alltags ) {

								// Find the passed tags and set them up.
								_.each( tags, function( tag ) {
									newTag = new wp.api.models.Tag( alltags.findWhere( { slug: tag } ) );

									// Tie the new tag to the post.
									newTag.set( 'parent_post', self.get( 'id' ) );

									// Add the new tag to the collection.
									newTags.push( newTag );
								} );
								tags = new wp.api.collections.Tags( newTags );
								self.setTagsWithCollection( tags );
							}
						} );

					} else {
						this.setTagsWithCollection( tags );
					}
				},

				/**
				 * Set the tags for a post.
				 *
				 * Accepts a Tags collection.
				 *
				 * @param {array|Backbone.Collection} tags The tags to set on the post.
				 *
				 */
				setTagsWithCollection: function( tags ) {

					// Pluck out the category ids.
					this.set( 'tags', tags.pluck( 'id' ) );
					return this.save();
				}
			},

			/**
			 * Add a helper function to handle post Categories.
			 */
			CategoriesMixin = {

				/**
				 * Get a the categories for a post.
				 *
				 * @return {Deferred.promise} promise Resolves to an array of categories.
				 */
				getCategories: function() {
					var categoryIds = this.get( 'categories' ),
						categories  = new wp.api.collections.Categories();

					// Resolve with an empty array if no categories.
					if ( _.isEmpty( categoryIds ) ) {
						return jQuery.Deferred().resolve( [] );
					}

					return categories.fetch( { data: { include: categoryIds } } );
				},

				/**
				 * Set the categories for a post.
				 *
				 * Accepts an array of category slugs, or a Categories collection.
				 *
				 * @param {array|Backbone.Collection} categories The categories to set on the post.
				 *
				 */
				setCategories: function( categories ) {
					var allCategories, newCategory,
						self = this,
						newCategories = [];

					if ( _.isString( categories ) ) {
						return false;
					}

					// If this is an array of slugs, build a collection.
					if ( _.isArray( categories ) ) {

						// Get all the categories.
						allCategories = new wp.api.collections.Categories();
						allCategories.fetch( {
							data:    { per_page: 100 },
							success: function( allcats ) {

								// Find the passed categories and set them up.
								_.each( categories, function( category ) {
									newCategory = new wp.api.models.Category( allcats.findWhere( { slug: category } ) );

									// Tie the new category to the post.
									newCategory.set( 'parent_post', self.get( 'id' ) );

									// Add the new category to the collection.
									newCategories.push( newCategory );
								} );
								categories = new wp.api.collections.Categories( newCategories );
								self.setCategoriesWithCollection( categories );
							}
						} );

					} else {
						this.setCategoriesWithCollection( categories );
					}

				},

				/**
				 * Set the categories for a post.
				 *
				 * Accepts Categories collection.
				 *
				 * @param {array|Backbone.Collection} categories The categories to set on the post.
				 *
				 */
				setCategoriesWithCollection: function( categories ) {

					// Pluck out the category ids.
					this.set( 'categories', categories.pluck( 'id' ) );
					return this.save();
				}
			},

			/**
			 * Add a helper function to retrieve the author user model.
			 */
			AuthorMixin = {
				getAuthorUser: function() {
					return buildModelGetter( this, this.get( 'author' ), 'User', 'author', 'name' );
				}
			},

			/**
			 * Add a helper function to retrieve the featured media.
			 */
			FeaturedMediaMixin = {
				getFeaturedMedia: function() {
					return buildModelGetter( this, this.get( 'featured_media' ), 'Media', 'wp:featuredmedia', 'source_url' );
				}
			};

		// Exit if we don't have valid model defaults.
		if ( _.isUndefined( model.prototype.args ) ) {
			return model;
		}

		// Go thru the parsable date fields, if our model contains any of them it gets the TimeStampedMixin.
		_.each( parseableDates, function( theDateKey ) {
			if ( ! _.isUndefined( model.prototype.args[ theDateKey ] ) ) {
				hasDate = true;
			}
		} );

		// Add the TimeStampedMixin for models that contain a date field.
		if ( hasDate ) {
			model = model.extend( TimeStampedMixin );
		}

		// Add the AuthorMixin for models that contain an author.
		if ( ! _.isUndefined( model.prototype.args.author ) ) {
			model = model.extend( AuthorMixin );
		}

		// Add the FeaturedMediaMixin for models that contain a featured_media.
		if ( ! _.isUndefined( model.prototype.args.featured_media ) ) {
			model = model.extend( FeaturedMediaMixin );
		}

		// Add the CategoriesMixin for models that support categories collections.
		if ( ! _.isUndefined( model.prototype.args.categories ) ) {
			model = model.extend( CategoriesMixin );
		}

		// Add the MetaMixin for models that support meta.
		if ( ! _.isUndefined( model.prototype.args.meta ) ) {
			model = model.extend( MetaMixin );
		}

		// Add the TagsMixin for models that support tags collections.
		if ( ! _.isUndefined( model.prototype.args.tags ) ) {
			model = model.extend( TagsMixin );
		}

		// Add the RevisionsMixin for models that support revisions collections.
		if ( ! _.isUndefined( loadingObjects.collections[ modelClassName + 'Revisions' ] ) ) {
			model = model.extend( RevisionsMixin );
		}

		return model;
	};

})( window );

/* global wpApiSettings:false */

// Suppress warning about parse function's unused "options" argument:
/* jshint unused:false */
(function() {

	'use strict';

	var wpApiSettings = window.wpApiSettings || {},
	trashableTypes    = [ 'Comment', 'Media', 'Comment', 'Post', 'Page', 'Status', 'Taxonomy', 'Type' ];

	/**
	 * Backbone base model for all models.
	 */
	wp.api.WPApiBaseModel = Backbone.Model.extend(
		/** @lends WPApiBaseModel.prototype  */
		{

			// Initialize the model.
			initialize: function() {

				/**
				* Types that don't support trashing require passing ?force=true to delete.
				*
				*/
				if ( -1 === _.indexOf( trashableTypes, this.name ) ) {
					this.requireForceForDelete = true;
				}
			},

			/**
			 * Set nonce header before every Backbone sync.
			 *
			 * @param {string} method.
			 * @param {Backbone.Model} model.
			 * @param {{beforeSend}, *} options.
			 * @returns {*}.
			 */
			sync: function( method, model, options ) {
				var beforeSend;

				options = options || {};

				// Remove date_gmt if null.
				if ( _.isNull( model.get( 'date_gmt' ) ) ) {
					model.unset( 'date_gmt' );
				}

				// Remove slug if empty.
				if ( _.isEmpty( model.get( 'slug' ) ) ) {
					model.unset( 'slug' );
				}

				if ( _.isFunction( model.nonce ) && ! _.isEmpty( model.nonce() ) ) {
					beforeSend = options.beforeSend;

					// @todo enable option for jsonp endpoints
					// options.dataType = 'jsonp';

					// Include the nonce with requests.
					options.beforeSend = function( xhr ) {
						xhr.setRequestHeader( 'X-WP-Nonce', model.nonce() );

						if ( beforeSend ) {
							return beforeSend.apply( this, arguments );
						}
					};

					// Update the nonce when a new nonce is returned with the response.
					options.complete = function( xhr ) {
						var returnedNonce = xhr.getResponseHeader( 'X-WP-Nonce' );

						if ( returnedNonce && _.isFunction( model.nonce ) && model.nonce() !== returnedNonce ) {
							model.endpointModel.set( 'nonce', returnedNonce );
						}
					};
				}

				// Add '?force=true' to use delete method when required.
				if ( this.requireForceForDelete && 'delete' === method ) {
					model.url = model.url() + '?force=true';
				}
				return Backbone.sync( method, model, options );
			},

			/**
			 * Save is only allowed when the PUT OR POST methods are available for the endpoint.
			 */
			save: function( attrs, options ) {

				// Do we have the put method, then execute the save.
				if ( _.includes( this.methods, 'PUT' ) || _.includes( this.methods, 'POST' ) ) {

					// Proxy the call to the original save function.
					return Backbone.Model.prototype.save.call( this, attrs, options );
				} else {

					// Otherwise bail, disallowing action.
					return false;
				}
			},

			/**
			 * Delete is only allowed when the DELETE method is available for the endpoint.
			 */
			destroy: function( options ) {

				// Do we have the DELETE method, then execute the destroy.
				if ( _.includes( this.methods, 'DELETE' ) ) {

					// Proxy the call to the original save function.
					return Backbone.Model.prototype.destroy.call( this, options );
				} else {

					// Otherwise bail, disallowing action.
					return false;
				}
			}

		}
	);

	/**
	 * API Schema model. Contains meta information about the API.
	 */
	wp.api.models.Schema = wp.api.WPApiBaseModel.extend(
		/** @lends Schema.prototype  */
		{
			defaults: {
				_links: {},
				namespace: null,
				routes: {}
			},

			initialize: function( attributes, options ) {
				var model = this;
				options = options || {};

				wp.api.WPApiBaseModel.prototype.initialize.call( model, attributes, options );

				model.apiRoot = options.apiRoot || wpApiSettings.root;
				model.versionString = options.versionString || wpApiSettings.versionString;
			},

			url: function() {
				return this.apiRoot + this.versionString;
			}
		}
	);
})();

( function() {

	'use strict';

	var wpApiSettings = window.wpApiSettings || {};

	/**
	 * Contains basic collection functionality such as pagination.
	 */
	wp.api.WPApiBaseCollection = Backbone.Collection.extend(
		/** @lends BaseCollection.prototype  */
		{

			/**
			 * Setup default state.
			 */
			initialize: function( models, options ) {
				this.state = {
					data: {},
					currentPage: null,
					totalPages: null,
					totalObjects: null
				};
				if ( _.isUndefined( options ) ) {
					this.parent = '';
				} else {
					this.parent = options.parent;
				}
			},

			/**
			 * Extend Backbone.Collection.sync to add nince and pagination support.
			 *
			 * Set nonce header before every Backbone sync.
			 *
			 * @param {string} method.
			 * @param {Backbone.Model} model.
			 * @param {{success}, *} options.
			 * @returns {*}.
			 */
			sync: function( method, model, options ) {
				var beforeSend, success,
					self = this;

				options = options || {};

				if ( _.isFunction( model.nonce ) && ! _.isEmpty( model.nonce() ) ) {
					beforeSend = options.beforeSend;

					// Include the nonce with requests.
					options.beforeSend = function( xhr ) {
						xhr.setRequestHeader( 'X-WP-Nonce', model.nonce() );

						if ( beforeSend ) {
							return beforeSend.apply( self, arguments );
						}
					};

					// Update the nonce when a new nonce is returned with the response.
					options.complete = function( xhr ) {
						var returnedNonce = xhr.getResponseHeader( 'X-WP-Nonce' );

						if ( returnedNonce && _.isFunction( model.nonce ) && model.nonce() !== returnedNonce ) {
							model.endpointModel.set( 'nonce', returnedNonce );
						}
					};
				}

				// When reading, add pagination data.
				if ( 'read' === method ) {
					if ( options.data ) {
						self.state.data = _.clone( options.data );

						delete self.state.data.page;
					} else {
						self.state.data = options.data = {};
					}

					if ( 'undefined' === typeof options.data.page ) {
						self.state.currentPage  = null;
						self.state.totalPages   = null;
						self.state.totalObjects = null;
					} else {
						self.state.currentPage = options.data.page - 1;
					}

					success = options.success;
					options.success = function( data, textStatus, request ) {
						if ( ! _.isUndefined( request ) ) {
							self.state.totalPages   = parseInt( request.getResponseHeader( 'x-wp-totalpages' ), 10 );
							self.state.totalObjects = parseInt( request.getResponseHeader( 'x-wp-total' ), 10 );
						}

						if ( null === self.state.currentPage ) {
							self.state.currentPage = 1;
						} else {
							self.state.currentPage++;
						}

						if ( success ) {
							return success.apply( this, arguments );
						}
					};
				}

				// Continue by calling Bacckbone's sync.
				return Backbone.sync( method, model, options );
			},

			/**
			 * Fetches the next page of objects if a new page exists.
			 *
			 * @param {data: {page}} options.
			 * @returns {*}.
			 */
			more: function( options ) {
				options = options || {};
				options.data = options.data || {};

				_.extend( options.data, this.state.data );

				if ( 'undefined' === typeof options.data.page ) {
					if ( ! this.hasMore() ) {
						return false;
					}

					if ( null === this.state.currentPage || this.state.currentPage <= 1 ) {
						options.data.page = 2;
					} else {
						options.data.page = this.state.currentPage + 1;
					}
				}

				return this.fetch( options );
			},

			/**
			 * Returns true if there are more pages of objects available.
			 *
			 * @returns null|boolean.
			 */
			hasMore: function() {
				if ( null === this.state.totalPages ||
					 null === this.state.totalObjects ||
					 null === this.state.currentPage ) {
					return null;
				} else {
					return ( this.state.currentPage < this.state.totalPages );
				}
			}
		}
	);

} )();

( function() {

	'use strict';

	var Endpoint, initializedDeferreds = {},
		wpApiSettings = window.wpApiSettings || {};

	/** @namespace wp */
	window.wp = window.wp || {};

	/** @namespace wp.api */
	wp.api    = wp.api || {};

	// If wpApiSettings is unavailable, try the default.
	if ( _.isEmpty( wpApiSettings ) ) {
		wpApiSettings.root = window.location.origin + '/wp-json/';
	}

	Endpoint = Backbone.Model.extend(/** @lends Endpoint.prototype */{
		defaults: {
			apiRoot: wpApiSettings.root,
			versionString: wp.api.versionString,
			nonce: null,
			schema: null,
			models: {},
			collections: {}
		},

		/**
		 * Initialize the Endpoint model.
		 */
		initialize: function() {
			var model = this, deferred;

			Backbone.Model.prototype.initialize.apply( model, arguments );

			deferred = jQuery.Deferred();
			model.schemaConstructed = deferred.promise();

			model.schemaModel = new wp.api.models.Schema( null, {
				apiRoot:       model.get( 'apiRoot' ),
				versionString: model.get( 'versionString' ),
				nonce:         model.get( 'nonce' )
			} );

			// When the model loads, resolve the promise.
			model.schemaModel.once( 'change', function() {
				model.constructFromSchema();
				deferred.resolve( model );
			} );

			if ( model.get( 'schema' ) ) {

				// Use schema supplied as model attribute.
				model.schemaModel.set( model.schemaModel.parse( model.get( 'schema' ) ) );
			} else if (
				! _.isUndefined( sessionStorage ) &&
				( _.isUndefined( wpApiSettings.cacheSchema ) || wpApiSettings.cacheSchema ) &&
				sessionStorage.getItem( 'wp-api-schema-model' + model.get( 'apiRoot' ) + model.get( 'versionString' ) )
			) {

				// Used a cached copy of the schema model if available.
				model.schemaModel.set( model.schemaModel.parse( JSON.parse( sessionStorage.getItem( 'wp-api-schema-model' + model.get( 'apiRoot' ) + model.get( 'versionString' ) ) ) ) );
			} else {
				model.schemaModel.fetch( {
					/**
					 * When the server returns the schema model data, store the data in a sessionCache so we don't
					 * have to retrieve it again for this session. Then, construct the models and collections based
					 * on the schema model data.
					 *
					 * @ignore
					 */
					success: function( newSchemaModel ) {

						// Store a copy of the schema model in the session cache if available.
						if ( ! _.isUndefined( sessionStorage ) && ( _.isUndefined( wpApiSettings.cacheSchema ) || wpApiSettings.cacheSchema ) ) {
							try {
								sessionStorage.setItem( 'wp-api-schema-model' + model.get( 'apiRoot' ) + model.get( 'versionString' ), JSON.stringify( newSchemaModel ) );
							} catch ( error ) {

								// Fail silently, fixes errors in safari private mode.
							}
						}
					},

					// Log the error condition.
					error: function( err ) {
						window.console.log( err );
					}
				} );
			}
		},

		constructFromSchema: function() {
			var routeModel = this, modelRoutes, collectionRoutes, schemaRoot, loadingObjects,

			/**
			 * Set up the model and collection name mapping options. As the schema is built, the
			 * model and collection names will be adjusted if they are found in the mapping object.
			 *
			 * Localizing a variable wpApiSettings.mapping will over-ride the default mapping options.
			 *
			 */
			mapping = wpApiSettings.mapping || {
				models: {
					'Categories':      'Category',
					'Comments':        'Comment',
					'Pages':           'Page',
					'PagesMeta':       'PageMeta',
					'PagesRevisions':  'PageRevision',
					'Posts':           'Post',
					'PostsCategories': 'PostCategory',
					'PostsRevisions':  'PostRevision',
					'PostsTags':       'PostTag',
					'Schema':          'Schema',
					'Statuses':        'Status',
					'Tags':            'Tag',
					'Taxonomies':      'Taxonomy',
					'Types':           'Type',
					'Users':           'User'
				},
				collections: {
					'PagesMeta':       'PageMeta',
					'PagesRevisions':  'PageRevisions',
					'PostsCategories': 'PostCategories',
					'PostsMeta':       'PostMeta',
					'PostsRevisions':  'PostRevisions',
					'PostsTags':       'PostTags'
				}
			},

			modelEndpoints = routeModel.get( 'modelEndpoints' ),
			modelRegex     = new RegExp( '(?:.*[+)]|\/(' + modelEndpoints.join( '|' ) + '))$' );

			/**
			 * Iterate thru the routes, picking up models and collections to build. Builds two arrays,
			 * one for models and one for collections.
			 */
			modelRoutes      = [];
			collectionRoutes = [];
			schemaRoot       = routeModel.get( 'apiRoot' ).replace( wp.api.utils.getRootUrl(), '' );
			loadingObjects   = {};

			/**
			 * Tracking objects for models and collections.
			 */
			loadingObjects.models      = {};
			loadingObjects.collections = {};

			_.each( routeModel.schemaModel.get( 'routes' ), function( route, index ) {

				// Skip the schema root if included in the schema.
				if ( index !== routeModel.get( ' versionString' ) &&
						index !== schemaRoot &&
						index !== ( '/' + routeModel.get( 'versionString' ).slice( 0, -1 ) )
				) {

					// Single items end with a regex, or a special case word.
					if ( modelRegex.test( index ) ) {
						modelRoutes.push( { index: index, route: route } );
					} else {

						// Collections end in a name.
						collectionRoutes.push( { index: index, route: route } );
					}
				}
			} );

			/**
			 * Construct the models.
			 *
			 * Base the class name on the route endpoint.
			 */
			_.each( modelRoutes, function( modelRoute ) {

				// Extract the name and any parent from the route.
				var modelClassName,
					routeName  = wp.api.utils.extractRoutePart( modelRoute.index, 2, routeModel.get( 'versionString' ), true ),
					parentName = wp.api.utils.extractRoutePart( modelRoute.index, 1, routeModel.get( 'versionString' ), false ),
					routeEnd   = wp.api.utils.extractRoutePart( modelRoute.index, 1, routeModel.get( 'versionString' ), true );

				// Clear the parent part of the rouite if its actually the version string.
				if ( parentName === routeModel.get( 'versionString' ) ) {
					parentName = '';
				}

				// Handle the special case of the 'me' route.
				if ( 'me' === routeEnd ) {
					routeName = 'me';
				}

				// If the model has a parent in its route, add that to its class name.
				if ( '' !== parentName && parentName !== routeName ) {
					modelClassName = wp.api.utils.capitalizeAndCamelCaseDashes( parentName ) + wp.api.utils.capitalizeAndCamelCaseDashes( routeName );
					modelClassName = mapping.models[ modelClassName ] || modelClassName;
					loadingObjects.models[ modelClassName ] = wp.api.WPApiBaseModel.extend( {

						// Return a constructed url based on the parent and id.
						url: function() {
							var url =
								routeModel.get( 'apiRoot' ) +
								routeModel.get( 'versionString' ) +
								parentName +  '/' +
									( ( _.isUndefined( this.get( 'parent' ) ) || 0 === this.get( 'parent' ) ) ?
										( _.isUndefined( this.get( 'parent_post' ) ) ? '' : this.get( 'parent_post' ) + '/' ) :
										this.get( 'parent' ) + '/' ) +
								routeName;

							if ( ! _.isUndefined( this.get( 'id' ) ) ) {
								url +=  '/' + this.get( 'id' );
							}
							return url;
						},

						// Track nonces on the Endpoint 'routeModel'.
						nonce: function() {
							return routeModel.get( 'nonce' );
						},

						endpointModel: routeModel,

						// Include a reference to the original route object.
						route: modelRoute,

						// Include a reference to the original class name.
						name: modelClassName,

						// Include the array of route methods for easy reference.
						methods: modelRoute.route.methods,

						// Include the array of route endpoints for easy reference.
						endpoints: modelRoute.route.endpoints
					} );
				} else {

					// This is a model without a parent in its route
					modelClassName = wp.api.utils.capitalizeAndCamelCaseDashes( routeName );
					modelClassName = mapping.models[ modelClassName ] || modelClassName;
					loadingObjects.models[ modelClassName ] = wp.api.WPApiBaseModel.extend( {

						// Function that returns a constructed url based on the id.
						url: function() {
							var url = routeModel.get( 'apiRoot' ) +
								routeModel.get( 'versionString' ) +
								( ( 'me' === routeName ) ? 'users/me' : routeName );

							if ( ! _.isUndefined( this.get( 'id' ) ) ) {
								url +=  '/' + this.get( 'id' );
							}
							return url;
						},

						// Track nonces at the Endpoint level.
						nonce: function() {
							return routeModel.get( 'nonce' );
						},

						endpointModel: routeModel,

						// Include a reference to the original route object.
						route: modelRoute,

						// Include a reference to the original class name.
						name: modelClassName,

						// Include the array of route methods for easy reference.
						methods: modelRoute.route.methods,

						// Include the array of route endpoints for easy reference.
						endpoints: modelRoute.route.endpoints
					} );
				}

				// Add defaults to the new model, pulled form the endpoint.
				wp.api.utils.decorateFromRoute(
					modelRoute.route.endpoints,
					loadingObjects.models[ modelClassName ],
					routeModel.get( 'versionString' )
				);

			} );

			/**
			 * Construct the collections.
			 *
			 * Base the class name on the route endpoint.
			 */
			_.each( collectionRoutes, function( collectionRoute ) {

				// Extract the name and any parent from the route.
				var collectionClassName, modelClassName,
						routeName  = collectionRoute.index.slice( collectionRoute.index.lastIndexOf( '/' ) + 1 ),
						parentName = wp.api.utils.extractRoutePart( collectionRoute.index, 1, routeModel.get( 'versionString' ), false );

				// If the collection has a parent in its route, add that to its class name.
				if ( '' !== parentName && parentName !== routeName && routeModel.get( 'versionString' ) !== parentName ) {

					collectionClassName = wp.api.utils.capitalizeAndCamelCaseDashes( parentName ) + wp.api.utils.capitalizeAndCamelCaseDashes( routeName );
					modelClassName      = mapping.models[ collectionClassName ] || collectionClassName;
					collectionClassName = mapping.collections[ collectionClassName ] || collectionClassName;
					loadingObjects.collections[ collectionClassName ] = wp.api.WPApiBaseCollection.extend( {

						// Function that returns a constructed url passed on the parent.
						url: function() {
							return routeModel.get( 'apiRoot' ) + routeModel.get( 'versionString' ) +
									parentName + '/' + this.parent + '/' +
									routeName;
						},

						// Specify the model that this collection contains.
						model: function( attrs, options ) {
							return new loadingObjects.models[ modelClassName ]( attrs, options );
						},

						// Track nonces at the Endpoint level.
						nonce: function() {
							return routeModel.get( 'nonce' );
						},

						endpointModel: routeModel,

						// Include a reference to the original class name.
						name: collectionClassName,

						// Include a reference to the original route object.
						route: collectionRoute,

						// Include the array of route methods for easy reference.
						methods: collectionRoute.route.methods
					} );
				} else {

					// This is a collection without a parent in its route.
					collectionClassName = wp.api.utils.capitalizeAndCamelCaseDashes( routeName );
					modelClassName      = mapping.models[ collectionClassName ] || collectionClassName;
					collectionClassName = mapping.collections[ collectionClassName ] || collectionClassName;
					loadingObjects.collections[ collectionClassName ] = wp.api.WPApiBaseCollection.extend( {

						// For the url of a root level collection, use a string.
						url: function() {
							return routeModel.get( 'apiRoot' ) + routeModel.get( 'versionString' ) + routeName;
						},

						// Specify the model that this collection contains.
						model: function( attrs, options ) {
							return new loadingObjects.models[ modelClassName ]( attrs, options );
						},

						// Track nonces at the Endpoint level.
						nonce: function() {
							return routeModel.get( 'nonce' );
						},

						endpointModel: routeModel,

						// Include a reference to the original class name.
						name: collectionClassName,

						// Include a reference to the original route object.
						route: collectionRoute,

						// Include the array of route methods for easy reference.
						methods: collectionRoute.route.methods
					} );
				}

				// Add defaults to the new model, pulled form the endpoint.
				wp.api.utils.decorateFromRoute( collectionRoute.route.endpoints, loadingObjects.collections[ collectionClassName ] );
			} );

			// Add mixins and helpers for each of the models.
			_.each( loadingObjects.models, function( model, index ) {
				loadingObjects.models[ index ] = wp.api.utils.addMixinsAndHelpers( model, index, loadingObjects );
			} );

			// Set the routeModel models and collections.
			routeModel.set( 'models', loadingObjects.models );
			routeModel.set( 'collections', loadingObjects.collections );

		}

	} );

	wp.api.endpoints = new Backbone.Collection();

	/**
	 * Initialize the wp-api, optionally passing the API root.
	 *
	 * @param {object} [args]
	 * @param {string} [args.nonce] The nonce. Optional, defaults to wpApiSettings.nonce.
	 * @param {string} [args.apiRoot] The api root. Optional, defaults to wpApiSettings.root.
	 * @param {string} [args.versionString] The version string. Optional, defaults to wpApiSettings.root.
	 * @param {object} [args.schema] The schema. Optional, will be fetched from API if not provided.
	 */
	wp.api.init = function( args ) {
		var endpoint, attributes = {}, deferred, promise;

		args                      = args || {};
		attributes.nonce          = _.isString( args.nonce ) ? args.nonce : ( wpApiSettings.nonce || '' );
		attributes.apiRoot        = args.apiRoot || wpApiSettings.root || '/wp-json';
		attributes.versionString  = args.versionString || wpApiSettings.versionString || 'wp/v2/';
		attributes.schema         = args.schema || null;
		attributes.modelEndpoints = args.modelEndpoints || [ 'me', 'settings' ];
		if ( ! attributes.schema && attributes.apiRoot === wpApiSettings.root && attributes.versionString === wpApiSettings.versionString ) {
			attributes.schema = wpApiSettings.schema;
		}

		if ( ! initializedDeferreds[ attributes.apiRoot + attributes.versionString ] ) {

			// Look for an existing copy of this endpoint
			endpoint = wp.api.endpoints.findWhere( { 'apiRoot': attributes.apiRoot, 'versionString': attributes.versionString } );
			if ( ! endpoint ) {
				endpoint = new Endpoint( attributes );
			}
			deferred = jQuery.Deferred();
			promise = deferred.promise();

			endpoint.schemaConstructed.done( function( resolvedEndpoint ) {
				wp.api.endpoints.add( resolvedEndpoint );

				// Map the default endpoints, extending any already present items (including Schema model).
				wp.api.models      = _.extend( wp.api.models, resolvedEndpoint.get( 'models' ) );
				wp.api.collections = _.extend( wp.api.collections, resolvedEndpoint.get( 'collections' ) );
				deferred.resolve( resolvedEndpoint );
			} );
			initializedDeferreds[ attributes.apiRoot + attributes.versionString ] = promise;
		}
		return initializedDeferreds[ attributes.apiRoot + attributes.versionString ];
	};

	/**
	 * Construct the default endpoints and add to an endpoints collection.
	 */

	// The wp.api.init function returns a promise that will resolve with the endpoint once it is ready.
	wp.api.loadPromise = wp.api.init();

} )();
