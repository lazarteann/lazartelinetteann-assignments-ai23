ić Chrome.Przeglądarki $1 nie możesz uruchamiać jako użytkownik root.Uruchom $1 jako normalny użytkownik. Jeśli jesteś programistą i chcesz skorzystać z konta roota, uruchom program jeszcze raz z flagą --no-sandbox.Ostrzeżenie: Twoje ustawienia $1 są zapisywane na dysku sieciowym. Może to spowalniać działanie, powodować awarie, a nawet doprowadzić do utraty danych.Nie można utworzyć katalogu danych$1 chce pobrać wiele plikówpobrać wiele plikówOstatnie karty{NUM_TABS,plural, =1{1 karta}few{# karty}many{# kart}other{# karty}}&HistoriapobraneOtwieram w istniejącej sesji przeglądarki.Twoje dane zostały zaszyfrowane z użyciem hasła Google w dniu $1. Wpisz je, by rozpocząć synchronizację.Zalogowano jako $1.Włączono synchronizację wszystkich danychWłączone – ustawienia niestandardoweBłąd logowaniaBłąd synchronizacjiDane logowania są nieaktualneZaloguj się ponownieWpisz hasło, by rozpocząć synchronizacjęWpisz hasłoSpróbuj zalogować się jeszcze razWyloguj się i zaloguj się ponownieOtwórz ustawieniaZaloguj się, by korzystać ze swoich zakładek, historii, haseł i innych ustawień na wszystkich urządzeniach. Będziesz też automatycznie logować się do usług Google, których używasz.Zaloguj się w $1Synchronizację wyłączył administrator.Synchronizacja została zatrzymana przez Panel Google.Potwierdź ustawienia synchronizacji, by ją rozpocząć.Zarządzaj zsynchronizowanymi danymi w <a href="$1" target="_blank">Panelu Google</a>.Uwierzytelnianie…Błąd podczas logowania.Ups, synchronizacja przestała działać.Synchronizacja nie działa. Spróbuj zalogować się ponownie.Synchronizacja nie działa. Wyloguj się i zaloguj się ponownie.Nie udało się połączyć z serwerem synchronizacji. Próbuję ponownie…Zaloguj się ponownie, by wznowić synchronizacjęTrwa konfigurowanie…Jesteś zalogowany(a) do $1. Twoje zakładki, historia i inne ustawienia są synchronizowane z Twoim kontem Google.Zaawansowane…Dane są szyfrowane Twoim hasłem synchronizacji. Nie obejmuje to form płatności ani adresów w Google Pay.Dane zostały zaszyfrowane Twoim hasłem synchronizacji w dniu $1.
          Nie obejmuje to form płatności ani adresów w Google Pay.Dane zostały zaszyfrowane Twoim hasłem Google z $1.
          Nie obejmuje to form płatności ani adresów w Google Pay.Nie jesteś zalogowany(a) do $1.(Nie wykorzystujesz wszystkich możliwości – $1)zaloguj sięPrzetłumaczyć tę stronę?Chcesz zobaczyć tłumaczenie Google tej strony z języka $1 na $2?PrzetłumaczonaNie udało się przetłumaczyć tej stronyZmień językiZawsze tłumacz z języka: $1Nigdy nie tłumacz z języka: $1Nigdy nie tłumacz tej witrynyTrwa tłumaczenie…Ta strona została przetłumaczona.Zawsze tłumaczTłumacz zawszeNie można przetłumaczyć tej strony.Język strony:Język tłumaczenia:Ustawienia języka$1 chce wysyłać Ci powiadomieniaPokazywanie powiadomieńWyślij wiadomość$1 chce używać lokalizacji Twojego komputeraSprawdzanie Twojej lokalizacjiTa strona zawiera elementy z następujących witryn, które śledzą Twoją lokalizację:Następującym witrynom zablokowano możliwość śledzenia Twojej lokalizacji na tej stronie:Ustawienia zostaną wyczyszczone przy następnym ponownym ładowaniu.Wyczyść te ustawienia na wypadek przyszłych wizytZarządzaj ustawieniami lokalizacjiTa strona śledzi Twoją lokalizację.Tej stronie zablokowano możliwość śledzenia Twojej lokalizacji.$1 chce mieć pełny dostęp do sterowania urządzeniami MIDIKorzystać z urządzeń MIDITa strona ma pełny dostęp do sterowania urządzeniami MIDI.Pełny dostęp do sterowania urządzeniami MIDI został zablokowany dla tej strony.Ta witryna ma pełny dostęp do sterowania urządzeniami MIDI.Pełny dostęp do sterowania urządzeniami MIDI został dla tej witryny zablokowany.Zarządzaj ustawieniami MIDI…Zarządzaj ustawieniami multimediów…Ta strona ma dostęp do kamery i mikrofonu.Ta strona ma zablokowany dostęp do kamery i mikrofonu.Ta strona ma dostęp do mikrofonu.Ta strona ma dostęp do kamery.Ta strona ma zablokowany dostęp do mikrofonu.Ta strona ma zablokowany dostęp do kamery.Zezwolono na dostęp do kamery i mikrofonuZablokowano dostęp do kamery i mikrofonuZezwolono na dostęp do mikrofonuZezwolono na dostęp do kameryZablokowano dostęp do mikrofonuZablokowano dostęp do kameryAby nowe ustawienia zaczęły działać, być może trzeba odświeżyć tę stronę.Swoje zapisane hasła znajdziesz tutaj: $1https://passwords.google.comKonta GoogleZapisano hasła do tej stronyNie zapisano żadnych haseł do tej stronyZapisane hasła do $1Nie zapisano żadnych haseł do $1Przywróć usunięte hasło użytkownika $1Usuń hasło użytkownika $1Logowanie się jakoŁatwo loguj się na różnych urządzeniachŁatwe logowanie$1 automatycznie loguje Cię w odpowiednich witrynach przy użyciu zapisanych wcześniej haseł.Dostęp do plików lokalnych na tym komputerze został wyłączony przez administratoraPliki graficznePliki dźwiękowePliki wideoPliki niestandardowePracujesz w trybie pełnoekranowym.Rozszerzenie $1 uruchomiło tryb pełnoekranowy.Rozszerzenie uruchomiło tryb pełnoekranowy.Strona $1 jest teraz wyświetlana w trybie pełnoekranowym.Ta strona jest wyświetlana w trybie pełnoekranowym.Strona $1 jest obecnie wyświetlana na pełnym ekranie i wyłączyła kursor myszy.Ta strona jest obecnie wyświetlana na pełnym ekranie i wyłączyła kursor myszy.Witryna $1 wyłączyła kursor myszy.Ta strona wyłączyła kursor myszy.Aby zamknąć pełny ekran, naciśnij |$1|Naciśnij |$1|, by wyświetlić kursorNaciśnij i przytrzymaj |$1|, by zamknąć pełny ekrane-mailkalendarz sieciowyZezwolić usłudze $1 na otwieranie wszystkich linków $2?Zezwolić usłudze $1 na otwieranie wszystkich linków $2 zamiast usługi $3?Otwieranie linków protokołu $1Otwieranie linków protokołu $1 zamiast modułu $2Ignoruj$1 chce korzystać z kamery i mikrofonu$1 chce korzystać z mikrofonu$1 chce korzystać z kamery$1 chce udostępnić Twój ekranTa witryna używa czujników ruchu lub oświetlenia.Tej witrynie zablokowano dostęp do czujników ruchu lub oświetlenia.Zawsze zezwalaj witrynie $1 na dostęp do czujnikówNadal blokuj dostęp do czujnikówZezwolono na dostęp do czujnikówZablokowano dostęp do czujnikówTa witryna ma dostęp do czujników ruchu lub oświetlenia.Ta witryna ma zablokowany dostęp do czujników ruchu i oświetlenia.Nadal zezwalaj na dostęp do czujnikówZawsze blokuj witrynie $1 dostęp do czujników$1 chce na stałe przechowywać dane na Twoim komputerze lokalnym$1 chce na stałe przechowywać dużą ilość danych na Twoim komputerzeZapisywanie plików na tym urządzeniuPrzeglądaj strony w trybie dużego kontrastuWypróbuj te rozwiązania:Rozszerzenie High ContrastCiemny motywKliknij Dalej, by wybrać domyślną przeglądarkę.Uprawnienia pliku multimedialnego dla rozszerzenia „$1”„$1” może odczytywać oraz zapisywać pliki graficzne, wideo i dźwiękowe z wybranych lokalizacji.„$1” może odczytywać oraz usuwać pliki graficzne, wideo i dźwiękowe ze sprawdzonych lokalizacji.„$1” może odczytywać pliki graficzne, wideo i dźwiękowe ze sprawdzonych lokalizacji.Sugestieostatnio podłączone w dniu $1Dodaj lokalizację…Dodaj galerię multimediów na podstawie katalogupodłączoneniepodłączoneNa stałe wyłącz dostęp wszystkich aplikacjiPamięć podręczna tokenów interfejsu API IdentityToken dostępuNazwa rozszerzeniaIdentyfikator rozszerzeniaStan tokenaNie znalezionoToken istniejeData ważnościZakresyUnieważnijSzybka, prosta i bezpieczna przeglądarka na miarę współczesnego internetu.Wykryto wysokie użycie dyskuAplikacja $1 używa $2 MB na dysku.Nie pokazuj już ostrzeżeń dla tej aplikacjiNie pokazuj już ostrzeżeń dla tego rozszerzeniaUsuń aplikację$1 chce udostępnić zawartość Twojego ekranu. Wybierz elementy, które chcesz udostępnić.$1 chce udostępnić zawartość Twojego ekranu stronie $2. Wybierz elementy, które chcesz udostępnić.Udostępnij dźwiękCały ekranOkno aplikacji{SCREEN_INDEX,plural, =1{Ekran #}few{Ekran #}many{s, your script should be enqueued as dependent
	 * on "quicktags" and outputted in the footer. If you are echoing JS directly from PHP,
	 * use add_action( 'admin_print_footer_scripts', 'output_my_js', 100 ) or add_action( 'wp_footer', 'output_my_js', 100 )
	 *
	 * Minimum required to add a button that calls an external function:
	 *     QTags.addButton( 'my_id', 'my button', my_callback );
	 *     function my_callback() { alert('yeah!'); }
	 *
	 * Minimum required to add a button that inserts a tag:
	 *     QTags.addButton( 'my_id', 'my button', '<span>', '</span>' );
	 *     QTags.addButton( 'my_id2', 'my button', '<br />' );
	 *
	 * @param string id Required. Button HTML ID
	 * @param string display Required. Button's value="..."
	 * @param string|function arg1 Required. Either a starting tag to be inserted like "<span>" or a callback that is executed when the button is clicked.
	 * @param string arg2 Optional. Ending tag like "</span>"
	 * @param string access_key Deprecated Not used
	 * @param string title Optional. Button's title="..."
	 * @param int priority Optional. Number representing the desired position of the button in the toolbar. 1 - 9 = first, 11 - 19 = second, 21 - 29 = third, etc.
	 * @param string instance Optional. Limit the button to a specific instance of Quicktags, add to all instances if not present.
	 * @param attr object Optional. Used to pass additional attributes. Currently supports `ariaLabel` and `ariaLabelClose` (for "close tag" state)
	 * @return mixed null or the button object that is needed for back-compat.
	 */
	qt.addButton = function( id, display, arg1, arg2, access_key, title, priority, instance, attr ) {
		var btn;

		if ( !id || !display ) {
			return;
		}

		priority = priority || 0;
		arg2 = arg2 || '';
		attr = attr || {};

		if ( typeof(arg1) === 'function' ) {
			btn = new qt.Button( id, display, access_key, title, instance, attr );
			btn.callback = arg1;
		} else if ( typeof(arg1) === 'string' ) {
			btn = new qt.TagButton( id, display, arg1, arg2, access_key, title, instance, attr );
		} else {
			return;
		}

		if ( priority === -1 ) { // back-compat
			return btn;
		}

		if ( priority > 0 ) {
			while ( typeof(edButtons[priority]) !== 'undefined' ) {
				priority++;
			}

			edButtons[priority] = btn;
		} else {
			edButtons[edButtons.length] = btn;
		}

		if ( this.buttonsInitDone ) {
			this._buttonsInit(); // add the button HTML to all instances toolbars if addButton() was called too late
		}
	};

	qt.insertContent = function(content) {
		var sel, startPos, endPos, scrollTop, text, canvas = document.getElementById(wpActiveEditor), event;

		if ( !canvas ) {
			return false;
		}

		if ( document.selection ) { //IE
			canvas.focus();
			sel = document.selection.createRange();
			sel.text = content;
			canvas.focus();
		} else if ( canvas.selectionStart || canvas.selectionStart === 0 ) { // FF, WebKit, Opera
			text = canvas.value;
			startPos = canvas.selectionStart;
			endPos = canvas.selectionEnd;
			scrollTop = canvas.scrollTop;

			canvas.value = text.substring(0, startPos) + content + text.substring(endPos, text.length);

			canvas.selectionStart = startPos + content.length;
			canvas.selectionEnd = startPos + content.length;
			canvas.scrollTop = scrollTop;
			canvas.focus();
		} else {
			canvas.value += content;
			canvas.focus();
		}

		if ( document.createEvent ) {
			event = document.createEvent( 'HTMLEvents' );
			event.initEvent( 'change', false, true );
			canvas.dispatchEvent( event );
		} else if ( canvas.fireEvent ) {
			canvas.fireEvent( 'onchange' );
		}

		return true;
	};

	// a plain, dumb button
	qt.Button = function( id, display, access, title, instance, attr ) {
		this.id = id;
		this.display = display;
		this.access = '';
		this.title = title || '';
		this.instance = instance || '';
		this.attr = attr || {};
	};
	qt.Button.prototype.html = function(idPrefix) {
		var active, on, wp,
			title = this.title ? ' title="' + _escape( this.title ) + '"' : '',
			ariaLabel = this.attr && this.attr.ariaLabel ? ' aria-label="' + _escape( this.attr.ariaLabel ) + '"' : '',
			val = this.display ? ' value="' + _escape( this.display ) + '"' : '',
			id = this.id ? ' id="' + _escape( idPrefix + this.id ) + '"' : '',
			dfw = ( wp = window.wp ) && wp.editor && wp.editor.dfw;

		if ( this.id === 'fullscreen' ) {
			return '<button type="button"' + id + ' class="ed_button qt-dfw qt-fullscreen"' + title + ariaLabel + '></button>';
		} else if ( this.id === 'dfw' ) {
			active = dfw && dfw.isActive() ? '' : ' disabled="disabled"';
			on = dfw && dfw.isOn() ? ' active' : '';

			return '<button type="button"' + id + ' class="ed_button qt-dfw' + on + '"' + title + ariaLabel + active + '></button>';
		}

		return '<input type="button"' + id + ' class="ed_button button button-small"' + title + ariaLabel + val + ' />';
	};
	qt.Button.prototype.callback = function(){};

	// a button that inserts HTML tag
	qt.TagButton = function( id, display, tagStart, tagEnd, access, title, instance, attr ) {
		var t = this;
		qt.Button.call( t, id, display, access, title, instance, attr );
		t.tagStart = tagStart;
		t.tagEnd = tagEnd;
	};
	qt.TagButton.prototype = new qt.Button();
	qt.TagButton.prototype.openTag = function( element, ed ) {
		if ( ! ed.openTags ) {
			ed.openTags = [];
		}

		if ( this.tagEnd ) {
			ed.openTags.push( this.id );
			element.value = '/' + element.value;

			if ( this.attr.ariaLabelClose ) {
				element.setAttribute( 'aria-label', this.attr.ariaLabelClose );
			}
		}
	};
	qt.TagButton.prototype.closeTag = function( element, ed ) {
		var i = this.isOpen(ed);

		if ( i !== false ) {
			ed.openTags.splice( i, 1 );
		}

		element.value = this.display;

		if ( this.attr.ariaLabel ) {
			element.setAttribute( 'aria-label', this.attr.ariaLabel );
		}
	};
	// whether a tag is open or not. Returns false if not open, or current open depth of the tag
	qt.TagButton.prototype.isOpen = function (ed) {
		var t = this, i = 0, ret = false;
		if ( ed.openTags ) {
			while ( ret === false && i < ed.openTags.length ) {
				ret = ed.openTags[i] === t.id ? i : false;
				i ++;
			}
		} else {
			ret = false;
		}
		return ret;
	};
	qt.TagButton.prototype.callback = function(element, canvas, ed) {
		var t = this, startPos, endPos, cursorPos, scrollTop, v = canvas.value, l, r, i, sel, endTag = v ? t.tagEnd : '', event;

		if ( document.selection ) { // IE
			canvas.focus();
			sel = document.selection.createRange();
			if ( sel.text.length > 0 ) {
				if ( !t.tagEnd ) {
					sel.text = sel.text + t.tagStart;
				} else {
					sel.text = t.tagStart + sel.text + endTag;
				}
			} else {
				if ( !t.tagEnd ) {
					sel.text = t.tagStart;
				} else if ( t.isOpen(ed) === false ) {
					sel.text = t.tagStart;
					t.openTag(element, ed);
				} else {
					sel.text = endTag;
					t.closeTag(element, ed);
				}
			}
			canvas.focus();
		} else if ( canvas.selectionStart || canvas.selectionStart === 0 ) { // FF, WebKit, Opera
			startPos = canvas.selectionStart;
			endPos = canvas.selectionEnd;

			if ( startPos < endPos && v.charAt( endPos - 1 ) === '\n' ) {
				endPos -= 1;
			}

			cursorPos = endPos;
			scrollTop = canvas.scrollTop;
			l = v.substring(0, startPos); // left of the selection
			r = v.substring(endPos, v.length); // right of the selection
			i = v.substring(startPos, endPos); // inside the selection
			if ( startPos !== endPos ) {
				if ( !t.tagEnd ) {
					canvas.value = l + i + t.tagStart + r; // insert self closing tags after the selection
					cursorPos += t.tagStart.length;
				} else {
					canvas.value = l + t.tagStart + i + endTag + r;
					cursorPos += t.tagStart.length + endTag.length;
				}
			} else {
				if ( !t.tagEnd ) {
					canvas.value = l + t.tagStart + r;
					cursorPos = startPos + t.tagStart.length;
				} else if ( t.isOpen(ed) === false ) {
					canvas.value = l + t.tagStart + r;
					t.openTag(element, ed);
					cursorPos = startPos + t.tagStart.length;
				} else {
					canvas.value = l + endTag + r;
					cursorPos = startPos + endTag.length;
					t.closeTag(element, ed);
				}
			}

			canvas.selectionStart = cursorPos;
			canvas.selectionEnd = cursorPos;
			canvas.scrollTop = scrollTop;
			canvas.focus();
		} else { // other browsers?
			if ( !endTag ) {
				canvas.value += t.tagStart;
			} else if ( t.isOpen(ed) !== false ) {
				canvas.value += t.tagStart;
				t.openTag(element, ed);
			} else {
				canvas.value += endTag;
				t.closeTag(element, ed);
			}
			canvas.focus();
		}

		if ( document.createEvent ) {
			event = document.createEvent( 'HTMLEvents' );
			event.initEvent( 'change', false, true );
			canvas.dispatchEvent( event );
		} else if ( canvas.fireEvent ) {
			canvas.fireEvent( 'onchange' );
		}
	};

	// removed
	qt.SpellButton = function() {};

	// the close tags button
	qt.CloseButton = function() {
		qt.Button.call( this, 'close', quicktagsL10n.closeTags, '', quicktagsL10n.closeAllOpenTags );
	};

	qt.CloseButton.prototype = new qt.Button();

	qt._close = function(e, c, ed) {
		var button, element, tbo = ed.openTags;

		if ( tbo ) {
			while ( tbo.length > 0 ) {
				button = ed.getButton(tbo[tbo.length - 1]);
				element = document.getElementById(ed.name + '_' + button.id);

				if ( e ) {
					button.callback.call(button, element, c, ed);
				} else {
					button.closeTag(element, ed);
				}
			}
		}
	};

	qt.CloseButton.prototype.callback = qt._close;

	qt.closeAllTags = function( editor_id ) {
		var ed = this.getInstance( editor_id );

		if ( ed ) {
			qt._close( '', ed.canvas, ed );
		}
	};

	// the link button
	qt.LinkButton = function() {
		var attr = {
			ariaLabel: quicktagsL10n.link
		};

		qt.TagButton.call( this, 'link', 'link', '', '</a>', '', '', '', attr );
	};
	qt.LinkButton.prototype = new qt.TagButton();
	qt.LinkButton.prototype.callback = function(e, c, ed, defaultValue) {
		var URL, t = this;

		if ( typeof wpLink !== 'undefined' ) {
			wpLink.open( ed.id );
			return;
		}

		if ( ! defaultValue ) {
			defaultValue = 'http://';
		}

		if ( t.isOpen(ed) === false ) {
			URL = prompt( quicktagsL10n.enterURL, defaultValue );
			if ( URL ) {
				t.tagStart = '<a href="' + URL + '">';
				qt.TagButton.prototype.callback.call(t, e, c, ed);
			}
		} else {
			qt.TagButton.prototype.callback.call(t, e, c, ed);
		}
	};

	// the img button
	qt.ImgButton = function() {
		var attr = {
			ariaLabel: quicktagsL10n.image
		};

		qt.TagButton.call( this, 'img', 'img', '', '', '', '', '', attr );
	};
	qt.ImgButton.prototype = new qt.TagButton();
	qt.ImgButton.prototype.callback = function(e, c, ed, defaultValue) {
		if ( ! defaultValue ) {
			defaultValue = 'http://';
		}
		var src = prompt(quicktagsL10n.enterImageURL, defaultValue), alt;
		if ( src ) {
			alt = prompt(quicktagsL10n.enterImageDescription, '');
			this.tagStart = '<img src="' + src + '" alt="' + alt + '" />';
			qt.TagButton.prototype.callback.call(this, e, c, ed);
		}
	};

	qt.DFWButton = function() {
		qt.Button.call( this, 'dfw', '', 'f', quicktagsL10n.dfw );
	};
	qt.DFWButton.prototype = new qt.Button();
	qt.DFWButton.prototype.callback = function() {
		var wp;

		if ( ! ( wp = window.wp ) || ! wp.editor || ! wp.editor.dfw ) {
			return;
		}

		window.wp.editor.dfw.toggle();
	};

	qt.TextDirectionButton = function() {
		qt.Button.call( this, 'textdirection', quicktagsL10n.textdirection, '', quicktagsL10n.toggleTextdirection );
	};
	qt.TextDirectionButton.prototype = new qt.Button();
	qt.TextDirectionButton.prototype.callback = function(e, c) {
		var isRTL = ( 'rtl' === document.getElementsByTagName('html')[0].dir ),
			currentDirection = c.style.direction;

		if ( ! currentDirection ) {
			currentDirection = ( isRTL ) ? 'rtl' : 'ltr';
		}

		c.style.direction = ( 'rtl' === currentDirection ) ? 'ltr' : 'rtl';
		c.focus();
	};

	// ensure backward compatibility
	edButtons[10]  = new qt.TagButton( 'strong', 'b', '<strong>', '</strong>', '', '', '', { ariaLabel: quicktagsL10n.strong, ariaLabelClose: quicktagsL10n.strongClose } );
	edButtons[20]  = new qt.TagButton( 'em', 'i', '<em>', '</em>', '', '', '', { ariaLabel: quicktagsL10n.em, ariaLabelClose: quicktagsL10n.emClose } );
	edButtons[30]  = new qt.LinkButton(); // special case
	edButtons[40]  = new qt.TagButton( 'block', 'b-quote', '\n\n<blockquote>', '</blockquote>\n\n', '', '', '', { ariaLabel: quicktagsL10n.blockquote, ariaLabelClose: quicktagsL10n.blockquoteClose } );
	edButtons[50]  = new qt.TagButton( 'del', 'del', '<del datetime="' + _datetime + '">', '</del>', '', '', '', { ariaLabel: quicktagsL10n.del, ariaLabelClose: quicktagsL10n.delClose } );
	edButtons[60]  = new qt.TagButton( 'ins', 'ins', '<ins datetime="' + _datetime + '">', '</ins>', '', '', '', { ariaLabel: quicktagsL10n.ins, ariaLabelClose: quicktagsL10n.insClose } );
	edButtons[70]  = new qt.ImgButton(); // special case
	edButtons[80]  = new qt.TagButton( 'ul', 'ul', '<ul>\n', '</ul>\n\n', '', '', '', { ariaLabel: quicktagsL10n.ul, ariaLabelClose: quicktagsL10n.ulClose } );
	edButtons[90]  = new qt.TagButton( 'ol', 'ol', '<ol>\n', '</ol>\n\n', '', '', '', { ariaLabel: quicktagsL10n.ol, ariaLabelClose: quicktagsL10n.olClose } );
	edButtons[100] = new qt.TagButton( 'li', 'li', '\t<li>', '</li>\n', '', '', '', { ariaLabel: quicktagsL10n.li, ariaLabelClose: quicktagsL10n.liClose } );
	edButtons[110] = new qt.TagButton( 'code', 'code', '<code>', '</code>', '', '', '', { ariaLabel: quicktagsL10n.code, ariaLabelClose: quicktagsL10n.codeClose } );
	edButtons[120] = new qt.TagButton( 'more', 'more', '<!--more-->\n\n', '', '', '', '', { ariaLabel: quicktagsL10n.more } );
	edButtons[140] = new qt.CloseButton();

})();

/**
 * Initialize new instance of the Quicktags editor
 */
window.quicktags = function(settings) {
	return new window.QTags(settings);
};

/**
 * Inserts content at the caret in the active editor (textarea)
 *
 * Added for back compatibility
 * @see QTags.insertContent()
 */
window.edInsertContent = function(bah, txt) {
	return window.QTags.insertContent(txt);
};

/**
 * Adds a button to all instances of the editor
 *
 * Added for back compatibility, use QTags.addButton() as it gives more flexibility like type of button, button placement, etc.
 * @see QTags.addButton()
 */
window.edButton = function(id, display, tagStart, tagEnd, access) {
	return window.QTags.addButton( id, display, tagStart, tagEnd, access, '', -1 );
};
